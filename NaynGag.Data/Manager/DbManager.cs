﻿using Dapper;
using NaynGag.Model.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NaynGag.Data.Manager
{
    public class SqlFilter
    {
        public dynamic FilterBag { get; set; }
    }
    public abstract class DbManager<T> : IDisposable where T : IEntity
    {
        private string _connectionString;

        public string ConnectionString
        {
            get
            {
                if (String.IsNullOrEmpty(_connectionString))
                {
                    _connectionString = ConfigurationManager.ConnectionStrings["NaynGagDB"].ConnectionString;
                }
                return _connectionString;
            }
            set
            {
                _connectionString = value;
            }
        }
        public long UserId { get; set; }

        private SqlConnection connection;
        protected SqlConnection Connection
        {
            get
            {
                if (connection == null || connection.State == System.Data.ConnectionState.Closed || connection.State == System.Data.ConnectionState.Broken)
                {
                    if (connection != null) connection.Dispose();
                    connection = new SqlConnection(ConnectionString);
                }
                return connection;
            }
        }

        public virtual void Dispose()
        {
            if (connection != null) connection.Dispose();
        }

        protected void SetDefaultEntityParametersOnSave(IEntity entity)
        {
            if (entity.Id == 0)
            {
                entity.CreatedDate = DateTime.Now;

            }
            else
            {
                entity.ModifiedDate = DateTime.Now;

            }
        }

        public Task<IEnumerable<dynamic>> Query(string sql, object queryParameters)
        {
            return Connection.QueryAsync<dynamic>(sql, queryParameters);
        }

        public Task<int> CreateAsync(T entity)
        {
            string entityName = typeof(T).Name;
            string sqlBody = "INSERT INTO [{0}]({1}) VALUES({2}); SELECT cast(SCOPE_IDENTITY() as INT) AS NewId";

            List<string> fields_1 = new List<string>();
            string[] excludedFieldsOnCreate = { entityName + "ID", "Id", "ModifiedDate", "ModifiedBy", "Role" };

            foreach (var prop in typeof(T).GetProperties())
            {
                // don't add excluded fields
                if (excludedFieldsOnCreate.Contains(prop.Name)) continue;
                fields_1.Add(prop.Name);
            }
            List<string> fields_2 = new List<string>();

            foreach (var prop in typeof(T).GetProperties())
            {
                // don't add excluded fields
                if (excludedFieldsOnCreate.Contains(prop.Name)) continue;
                fields_2.Add("@" + prop.Name);
            }
            string sql = String.Format(sqlBody, entityName, string.Join(",", fields_1), string.Join(",", fields_2));

            SetDefaultEntityParametersOnSave(entity);
            return Connection.ExecuteScalarAsync<int>(sql, entity);
        }
        public int Create(T entity)
        {
            string entityName = typeof(T).Name;
            string sqlBody = "INSERT INTO [{0}]({1}) VALUES({2}); SELECT cast(SCOPE_IDENTITY() as INT) AS NewId";

            List<string> fields_1 = new List<string>();
            string[] excludedFieldsOnCreate = { entityName + "ID", "Id", "ModifiedDate", "ModifiedBy" };

            foreach (var prop in typeof(T).GetProperties())
            {
                // don't add excluded fields
                if (excludedFieldsOnCreate.Contains(prop.Name)) continue;
                fields_1.Add(prop.Name);
            }
            List<string> fields_2 = new List<string>();

            foreach (var prop in typeof(T).GetProperties())
            {
                // don't add excluded fields
                if (excludedFieldsOnCreate.Contains(prop.Name)) continue;
                fields_2.Add("@" + prop.Name);
            }
            string sql = String.Format(sqlBody, entityName, string.Join(",", fields_1), string.Join(",", fields_2));
            SetDefaultEntityParametersOnSave(entity);
            return Connection.ExecuteScalar<int>(sql, entity);
        }
        public Task UpdateAsync(T entity)
        {
            string entityName = typeof(T).Name;
            string sqlBody = "UPDATE [{0}] SET {1} WHERE {0}ID=@{0}ID";

            List<string> fields = new List<string>();
            string[] excludedFieldsOnCreate = { entityName + "ID", "Id", "CreatedDate", "CreatedBy" };

            foreach (var prop in typeof(T).GetProperties())
            {
                // don't add excluded fields
                if (excludedFieldsOnCreate.Contains(prop.Name)) continue;
                fields.Add(String.Format("{0}=@{0}", prop.Name));
            }
            string sql = String.Format(sqlBody, entityName, string.Join(",", fields));

            SetDefaultEntityParametersOnSave(entity);

            return Connection.ExecuteAsync(sql, entity);
        }
        public void Update(T entity)
        {
            string entityName = typeof(T).Name;
            string sqlBody = "UPDATE [{0}] SET {1} WHERE {0}ID=@{0}ID";

            List<string> fields = new List<string>();
            string[] excludedFieldsOnCreate = { entityName + "ID", "Id", "CreatedDate", "CreatedBy", "OrganizationID", "Role" };

            foreach (var prop in typeof(T).GetProperties())
            {
                // don't add excluded fields
                if (excludedFieldsOnCreate.Contains(prop.Name)) continue;
                fields.Add(String.Format("{0}=@{0}", prop.Name));
            }
            string sql = String.Format(sqlBody, entityName, string.Join(",", fields));

            SetDefaultEntityParametersOnSave(entity);

            Connection.Execute(sql, entity);
        }
        public Task<T> GetByIdAsync(int id)
        {
            string entityName = typeof(T).Name;
            string sql = String.Format("SELECT * FROM [{0}] WHERE {0}ID={1}", entityName, id);
            var result = Connection.Query<T>(sql).FirstOrDefault();
            return Task.FromResult<T>(result);
        }
        public T GetById(int id)
        {
            string entityName = typeof(T).Name;
            string sql = String.Format("SELECT * FROM [{0}] WHERE {0}ID={1}", entityName, id);
            var result = Connection.Query<T>(sql).FirstOrDefault();
            return result;
        }
        public Task DeleteAsync(int id, int userId)
        {
            string entityName = typeof(T).Name;
            string sql = String.Format("UPDATE [{0}] SET ActiveStatus=0 , ModifiedBy={2} , ModifiedDate=GETDATE() WHERE {0}ID={1}", entityName, id, userId);
            return Connection.ExecuteAsync(sql);
        }
        public void Delete(int id, int userId)
        {
            string entityName = typeof(T).Name;
            string sql = String.Format("UPDATE [{0}] SET ActiveStatus=0 , ModifiedBy={2} , ModifiedDate=GETDATE() WHERE {0}ID={1}", entityName, id, userId);
            Connection.Execute(sql);
        }
        public Task DeleteMultipleAsync(params int[] ids)
        {
            if (ids == null || ids.Length == 0) throw new ArgumentException("At least one item must be specified.", "ids");
            string entityName = typeof(T).Name;
            string sql = String.Format("UPDATE [{0}] SET IsDeleted=1 WHERE {0}ID IN @ids", entityName);
            return Connection.ExecuteAsync(sql, new { ids = ids });
        }
    }
}
