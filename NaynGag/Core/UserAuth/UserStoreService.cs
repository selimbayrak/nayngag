﻿using Microsoft.AspNet.Identity;
using NaynGag.Data.Repository;
using NaynGag.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace NaynGag.Core.UserAuth
{
    public class UserStoreService : IUserStore<ViewModelUser>,
     IUserPasswordStore<ViewModelUser>
    {
        UserRepository _context;
        public UserStoreService(UserRepository context)
        {
            _context = context;
        }

        public Task CreateAsync(ViewModelUser user)
        {
            throw new NotImplementedException();
        }
        public Task DeleteAsync(ViewModelUser user)
        {
            throw new NotImplementedException();
        }
        public Task<ViewModelUser> FindByIdAsync(string userId)
        {
            throw new NotImplementedException();
        }

        public Task<ViewModelUser> FindByNameAsync(string userName)
        {


            Task<ViewModelUser> task =
            _context.GetUserByUserName(userName);

            return task;
        }
        public Task<ViewModelUser> FindAsync(string userName, string password)
        {
            return null;
        }
        public Task UpdateAsync(ViewModelUser user)
        {
            throw new NotImplementedException();
        }
        public void Dispose()
        {
        }
        public Task<string> GetPasswordHashAsync(ViewModelUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return Task.FromResult(user.Password);
        }
        public Task<bool> HasPasswordAsync(ViewModelUser user)
        {
            return Task.FromResult(user.Password != null);
        }
        public Task SetPasswordHashAsync(
          ViewModelUser user, string passwordHash)
        {
            throw new NotImplementedException();
        }
    }
}