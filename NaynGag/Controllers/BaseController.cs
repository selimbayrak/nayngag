﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using NaynGag.Core.UserAuth;
using NaynGag.Data.Repository;
using NaynGag.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NaynGag.Controllers
{
    public class BaseController : Controller
    {
        public UserManager<ViewModelUser> UserManager;
        public BaseController()
        {
            var userManager =
          new UserManager<ViewModelUser>(new UserStoreService(new UserRepository()));
            userManager.PasswordHasher =
                      new MyPasswordHasher();
            UserManager = userManager;
        }
        public ActionResult LogOut()
        {
            GetAuthenticationManager().SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
        protected override void Dispose(bool disposing)
        {
            UserManager.Dispose();
            base.Dispose(disposing);
        }
        public IAuthenticationManager GetAuthenticationManager()
        {
            //var ctx = Request.GetOwinContext();
            //return ctx.Authentication;
            return null;
        }
        public void CreateCookie(string name, string value)
        {
            HttpCookie cookieVisitor = new HttpCookie(name, value);
            // cookieVisitor.Expires = DateTime.Now.AddDays(2);
            Response.Cookies.Add(cookieVisitor);
        }
        public string GetCookie(string name)
        {
            //Böyle bir cookie mevcut mu kontrol ediyoruz
            if (Request.Cookies.AllKeys.Contains(name))
            {
                //böyle bir cookie varsa bize geri değeri döndürsün
                return Request.Cookies[name].Value;
            }
            return null;
        }
        public void DeleteCookie(string name)
        {
            //Böyle bir cookie var mı kontrol ediyoruz
            if (GetCookie(name) != null)
            {
                //Varsa cookiemizi temizliyoruz
                Response.Cookies.Remove(name);
                //ya da 
                Response.Cookies[name].Expires = DateTime.Now.AddDays(-1);
            }
        }
    }
}