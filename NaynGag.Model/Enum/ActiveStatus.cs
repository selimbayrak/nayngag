﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaynGag.Model.Enum
{
    public enum ActiveStatus : int
    {
        Deleted = -1,
        Passive = 0,
        Active = 1
    }
}
