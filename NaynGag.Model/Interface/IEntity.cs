﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaynGag.Model.Interface
{
    public interface IEntity
    {
        int Id { get; }
        int? CreatedBy { get; set; }
        int? ModifiedBy { get; set; }
        DateTime? CreatedDate { get; set; }
        DateTime? ModifiedDate { get; set; }
        string IPAddress { get; set; }
        Enum.ActiveStatus ActiveStatus { get; set; }
    }
}
