﻿using Microsoft.AspNet.Identity;
using NaynGag.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaynGag.Model.ViewModel
{
    public class ViewModelUser : IUser
    {
        public string Id { get { return UserID.ToString(); } }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsLockedOut { get; set; }
        public int FailedPasswordAttemptCount { get; set; }
        public int RoleID { get; set; }
        public string Permission { get; set; }
        public ActiveStatus ActiveStatus { get; set; }
    }
}
