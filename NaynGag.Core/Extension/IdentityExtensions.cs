﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace NaynGag.Core.Extension
{
    public static class IdentityExtensions
    {
        public static int GetUserID(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("UserId");
            return (claim != null) ? Convert.ToInt32(claim.Value) : 0;
        }
        public static string GetUserName(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("UserName");
            return (claim != null) ? claim.Value : string.Empty;
        }
        public static string GetSyncDllName(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("SyncDllName");
            return (claim != null) ? claim.Value : string.Empty;
        }
        public static int GetRole(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("Role");
            return (claim != null) ? Convert.ToInt32(claim.Value) : 0;
        }
        public static string[] GetPermission(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("Permissions");
            return (claim != null) ? claim.Value.Split(',') : new string[0];
        }
        public static int GetOrganizationId(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("OrganizationId");
            return (claim != null) ? Convert.ToInt32(claim.Value) : 0;
        }
        //public static bool HasPermission(this IIdentity identity, SecurityPermissionType type)
        //{
        //    if (GetPermission(identity).Contains(((int)type).ToString()))
        //        return true;
        //    else
        //        return false;
        //}
    }
}
